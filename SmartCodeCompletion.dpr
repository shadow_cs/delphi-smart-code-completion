{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

library SmartCodeCompletion;

uses
  SysUtils,
  ToolsAPI,
  SmartCodeCompletion.Expert in 'Source\SmartCodeCompletion.Expert.pas',
  Levenshtein in 'Source\Levenshtein.pas',
  SmartCodeCompletion.Wrapper in 'Source\SmartCodeCompletion.Wrapper.pas',
  SmartCodeCompletion.Logging in 'Source\SmartCodeCompletion.Logging.pas';

{$R *.res}

procedure FreePlugin;
begin
  Plugin.Free;
end;

function InitPlugin(const BorlandIDEServices: IBorlandIDEServices;
  RegisterProc: TWizardRegisterProc;
  var Terminate: TWizardTerminateProc): Boolean; stdcall;
begin
  try
    Plugin := TSmartCodeCompletion.Create;
    Terminate := FreePlugin;
  except
    on EAbort do
    begin
      Plugin := nil;
    end;
    else raise
  end;
  Result := True;
end;

exports
  InitPlugin name WizardEntryPoint;

end.
