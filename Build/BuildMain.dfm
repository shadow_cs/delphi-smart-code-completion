object frmMain: TfrmMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Install SmartCodeCompletion'
  ClientHeight = 205
  ClientWidth = 291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lstCompilers: TCheckListBox
    Left = 8
    Top = 8
    Width = 194
    Height = 189
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 0
  end
  object cmdInstall: TButton
    Left = 208
    Top = 8
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = '&Install'
    TabOrder = 1
    OnClick = cmdInstallClick
    ExplicitLeft = 190
  end
  object cmdUninstall: TButton
    Left = 208
    Top = 39
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = '&Uninstall'
    TabOrder = 2
    OnClick = cmdUninstallClick
    ExplicitLeft = 190
  end
  object cmdExit: TButton
    Left = 208
    Top = 172
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'E&xit'
    TabOrder = 3
    OnClick = cmdExitClick
    ExplicitLeft = 190
  end
end
