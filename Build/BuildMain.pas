{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

// Acknowledgement:
//  Few parts took inspiration in Spring4D build mechanism.
//  (https://bitbucket.org/sglienke/spring4d)

unit BuildMain;

interface

uses
  System.SysUtils,
  System.Variants,
  System.Classes,
  System.IniFiles,
  System.IOUtils,
  System.Win.Registry,
  Winapi.Windows,
  Winapi.Messages,
  Winapi.UserEnv,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.CheckLst;

type
  TfrmMain = class(TForm)
    lstCompilers: TCheckListBox;
    cmdInstall: TButton;
    cmdUninstall: TButton;
    cmdExit: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cmdExitClick(Sender: TObject);
    procedure cmdUninstallClick(Sender: TObject);
    procedure cmdInstallClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

type
  TSubkeyEnumerator = class;
  TSubkeyEnumerable = record
  strict private
    fRegistry: TRegistry;
    fKey, fBaseKey: string;
  public
    constructor Create(const registry: TRegistry; const key, baseKey: string);
    function GetEnumerator: TSubkeyEnumerator;
  end;

  TSubkeyEnumerator = class
  strict private
    fRegistry: TRegistry;
    fKey, fBaseKey: string;
    fKeyChanged: Boolean;
  public
    constructor Create(const registry: TRegistry; const key, baseKey: string);
    destructor Destroy; override;

    function MoveNext: Boolean;
    property Current: TRegistry read fRegistry;
  end;

  TTarget = class
  strict private
    fDisplayName, fProject, fBaseKey, fRootDir, fAppPath, fOutput: string;
    fRegistry: TRegistry;
  private
    function GetInstalled: Boolean;
    function Subkey(const subkey: string): TSubkeyEnumerable;
    procedure SetInstalled(const Value: Boolean);
  public
    constructor Create(const ini: TIniFile; const section: string);
    destructor Destroy; override;

    procedure Build;

    property DisplayName: string read fDisplayName;
    property Installed: Boolean read GetInstalled write SetInstalled;
  end;

  TIniFileHelper = class helper for TIniFile
    function ValuesExist(const section: string; const names: array of string): Boolean;
  end;

{ TTarget }

function GetSystemDirectory: string;
var
  len: Integer;
begin
  len := Winapi.Windows.GetSystemDirectory(nil, 0);
  if len > 0 then
  begin
    SetLength(Result, len);
    len := Winapi.Windows.GetSystemDirectory(PChar(Result), len);
    SetLength(Result, len);
  end
  else
    Result := '';
end;

procedure ExecuteCommandLine(const applicationName, commandLine: string;
  var exitCode: Cardinal; const workingDir: string);
var
  localCommandLine: string;
  startupInfo: TStartupInfo;
  processInfo: TProcessInformation;
  currentDir: PChar;
begin
  startupInfo := Default(TStartupInfo);
  processInfo := Default(TProcessInformation);
  startupInfo.cb := SizeOf(startupInfo);
  localCommandLine := commandLine;
  UniqueString(localCommandLine);
  if workingDir <> '' then
    currentDir := PChar(workingDir)
  else
    currentDir := nil;
  if not CreateProcess(PChar(applicationName), PChar(localCommandLine),
    nil, nil, True, 0, nil, currentDir, startupInfo, processInfo) then
  begin
    raise Exception.CreateFmt('Failed to create process %s', [applicationName]);
  end;
  try
    WaitForSingleObject(processInfo.hProcess, INFINITE);
    GetExitCodeProcess(processInfo.hProcess, exitCode);
  finally
    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);
  end;
end;

procedure TTarget.Build;
var
  cmdFileName, rsVars, commandLine, pause: string;
  exitCode: Cardinal;
begin
{$IFDEF DEBUG}
  pause := ' pause';
{$ENDIF}
  cmdFileName := TPath.Combine(GetSystemDirectory, 'cmd.exe');
  rsVars := TPath.Combine(fRootDir, 'bin\rsvars.bat');
  commandLine := Format('/C BuildHelper "%0:s" "%1:s" "Config=Release" "Platform=Win32"' + pause, [
      rsVars, fProject]);
  ExecuteCommandLine(cmdFileName, commandLine, exitCode, TDirectory.GetCurrentDirectory);
  if exitCode <> 0 then
    raise Exception.CreateFmt('Build failed %s', [fProject]);
end;

constructor TTarget.Create(const ini: TIniFile; const section: string);
begin
  inherited Create;
  fDisplayName := ini.ReadString(section, 'DisplayName', section);
  fProject := ini.ReadString(section, 'Project', '');
  fRegistry := TRegistry.Create;
  fRegistry.RootKey := HKEY_CURRENT_USER;
  fBaseKey := '\' + ini.ReadString(section, 'BDS', '');
  if not fRegistry.OpenKey(fBaseKey, false) then
    raise Exception.Create('Cannot open key');
  fRootDir := fRegistry.ReadString('RootDir');
  fAppPath := fRegistry.ReadString('App');
  fOutput := TPath.Combine(TDirectory.GetCurrentDirectory, ini.ReadString(section, 'Output', ''))
end;

destructor TTarget.Destroy;
begin
  fRegistry.Free;
  inherited;
end;

function TTarget.GetInstalled: Boolean;
var
  r: TRegistry;
begin
  Result := False;
  for r in Subkey('Experts') do
    Result := r.ValueExists('SmartCodeCompletion');
end;

procedure TTarget.SetInstalled(const Value: Boolean);
var
  r: TRegistry;
begin
  for r in Subkey('Experts') do
  begin
    if Value then
      r.WriteString('SmartCodeCompletion', fOutput)
    else
      r.DeleteValue('SmartCodeCompletion')
  end;
end;

function TTarget.Subkey(const subkey: string): TSubkeyEnumerable;
begin
  Result := TSubkeyEnumerable.Create(fRegistry, subkey, fBaseKey);
end;

{ TIniFileHelper }

function TIniFileHelper.ValuesExist(const section: string;
  const names: array of string): Boolean;
var
  name: string;
begin
  for name in names do
    if not ValueExists(section, name) then
      Exit(False);
  Result := True;
end;

{ TfrmMain }

procedure TfrmMain.cmdExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.cmdInstallClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lstCompilers.Count - 1 do
    if lstCompilers.Checked[i] then
      with TTarget(lstCompilers.Items.Objects[i]) do
  begin
    Build;
    Installed := True;
  end;
end;

procedure TfrmMain.cmdUninstallClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lstCompilers.Count - 1 do
    if lstCompilers.Checked[i] then
      TTarget(lstCompilers.Items.Objects[i]).Installed := False;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lstCompilers.Count - 1 do
    lstCompilers.Items.Objects[i].Free;
end;

procedure TfrmMain.FormShow(Sender: TObject);
var
  ini: TIniFile;
  sections, keys: TStrings;
  registry: TRegistry;
  section: string;
  target: TTarget;
  index: Integer;
begin
  ini := TIniFile.Create(TPath.Combine(TDirectory.GetCurrentDirectory, 'Build.ini'));
  try
    sections := TStringList.Create;
    keys := TStringList.Create;
    registry := TRegistry.Create;
    try
      registry.RootKey := HKEY_CURRENT_USER;
      ini.ReadSections(sections);
      for section in sections do
        if ini.ValuesExist(section, ['Project', 'BDS', 'Output']) then
        if registry.KeyExists(ini.ReadString(section, 'BDS', '')) then
      begin
        target := TTarget.Create(ini, section);
        index := lstCompilers.Items.AddObject(target.DisplayName, target);
        lstCompilers.Checked[index] := target.Installed;
      end;
    finally
      sections.Free;
      keys.Free;
      registry.Free;
    end;
  finally
    ini.Free;
  end;
end;

{ TSubkeyEnumerable }

constructor TSubkeyEnumerable.Create(const registry: TRegistry; const key,
  baseKey: string);
begin
  fRegistry := registry;
  fKey := key;
  fBaseKey := baseKey;
end;

function TSubkeyEnumerable.GetEnumerator: TSubkeyEnumerator;
begin
  Result:=TSubkeyEnumerator.Create(fRegistry, fKey, fBaseKey);
end;

{ TSubkeyEnumerator }

constructor TSubkeyEnumerator.Create(const registry: TRegistry; const key,
  baseKey: string);
begin
  fRegistry := registry;
  fKey := key;
  fBaseKey := baseKey;
end;

destructor TSubkeyEnumerator.Destroy;
begin
  if fKeyChanged then
    fRegistry.OpenKey(fBaseKey, False);

  inherited;
end;

function TSubkeyEnumerator.MoveNext: Boolean;
begin
  Result := fRegistry.OpenKey(fKey, False);
  if Result then
    fKeyChanged := True;
end;

end.
