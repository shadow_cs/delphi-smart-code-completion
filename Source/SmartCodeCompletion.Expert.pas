{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

unit SmartCodeCompletion.Expert;

interface

{$I SmartCodeCompletion.inc}

{$IFDEF DEBUG}
  {$DEFINE WRAP_ALL}
{$ENDIF}

uses
  SysUtils,
  StrUtils,
  Classes,
  Generics.Defaults,
  Generics.Collections,
  Rtti,
  ToolsAPI,
  Windows,
  Graphics,
  Levenshtein,
  SmartCodeCompletion.Logging,
  SmartCodeCompletion.Wrapper;

type
  // Missing in ToolsAPI.pas
  IPascalSymbolList = interface
    ['{0F6FC4F5-0E9A-44C0-B684-57A6F7C76AF3}']
    function IsActive: Boolean;
  end;

  IOTACodeCompletionHistory = interface(IInterface)
    ['{D9BE3F58-D9D3-4CA8-96BE-ED069AA05912}']
    function GetPath: string;
    function GetValue: string;
  end;

  IOTAAutoCodeCompletionTrait = interface(IInterface)
    ['{A84C137A-F375-478E-AD0C-479B707A22EB}']
    function IdentFirstSet: TSysCharSet;
    function IdentPrevSet: TSysCharSet;
    function GetAutoCodeCompleteEnabled: Boolean;
  end;

  TSmartCodeInsightManagerWrapper = class;
  TSmartCodeInsightSymbolListWrapper = class;

  /// <summary>
  ///   Singleton holding expert specific data and some global references.
  /// </summary>
  TSmartCodeCompletion = class
  strict private
    fBitmap: HBITMAP;
    fAboutBoxIndex: Integer;
    [Unsafe] fManager: TSmartCodeInsightManagerWrapper;
  private
    fGetViewer_Trampoline: procedure(inst: Pointer; var viewer: IOTACodeInsightViewer);
    function WriteProcess(const adr: PPointer; const data: Pointer): Pointer;

    procedure ManagerFreed;
  public
    constructor Create;
    destructor Destroy; override;

    property Manager: TSmartCodeInsightManagerWrapper read fManager;
  end;

  TSmartCodeInsightPrimaryManagerWrapper = class(
    TAggregatedIDispatchWrapper<IOTAPrimaryCodeInsightManager>,
    IOTAPrimaryCodeInsightManager)
  public
    function GetContext: TOTACodeCompletionContext;
  end;

  TSmartCodeInsightCustomDrawCodeInsightViewerWrapper = class(
    TAggregatedInterfaceWrapper<INTACustomDrawCodeInsightViewer>,
    INTACustomDrawCodeInsightViewer)
  public
    procedure DrawLine(Index: Integer; Canvas: TCanvas; var Rect: TRect;
      DrawingHintText: Boolean; DoDraw: Boolean; var DefaultDraw: Boolean);
  end;

  TSmartCodeInsightAutoCodeCompletionTraitWrapper = class(
    TAggregatedInterfaceWrapper<IOTAAutoCodeCompletionTrait>,
    IOTAAutoCodeCompletionTrait)
  public
    function IdentFirstSet: TSysCharSet;
    function IdentPrevSet: TSysCharSet;
    function GetAutoCodeCompleteEnabled: Boolean;
  end;

  TSmartCodeInsightCodeInsightViewer = class(TInterfaceWrapper<IOTACodeInsightViewer>,
    IOTACodeInsightViewer, IOTACodeInsightViewer90)
  strict private
    [Unsafe] fManager: TSmartCodeInsightManagerWrapper;
  protected
    function GetIndex(Index: Integer): Integer; inline;
  public
    constructor Create(const instance: IOTACodeInsightViewer;
      const manager: TSmartCodeInsightManagerWrapper);

    //IOTACodeInsightViewer90
    function GetSelected(Index: Integer): Boolean;
    function GetItemCount: Integer;
    function GetSelectedString: string;
    function GetSelectedIndex: Integer;
    function GetCloseKey: Char;
    function GetIsValidSelection: Boolean;

    // IOTACodeInsightViewer
    function GetManagerIsValidSelection(const Mgr: IOTACodeInsightManager): Boolean; overload;
    function GetManagerIsValidSelection(const Mgr: IOTACodeInsightManager; Index: Integer): Boolean; overload;
    function GetManagerSelectedIndex(const Mgr: IOTACodeInsightManager): Integer;
  end;

  /// <summary>
  ///   Replacement wrapper for the default Delphi code insight manager. Always
  ///   only one instance exists and is strongly referenced by the code insight
  ///   services and weakly referenced by the <see cref="SmartCodeCompletion.Expert|TSmartCodeCompletion" />
  ///   .
  /// </summary>
  TSmartCodeInsightManagerWrapper = class(TInterfaceWrapper<IOTACodeInsightManager>,
    IOTACodeInsightManager, IOTACodeInsightManager90, IOTACodeInsightManager100,
    INTACustomDrawCodeInsightViewer
{$IFDEF WRAP_ALL}
    ,IOTAPrimaryCodeInsightManager
    ,IOTAAutoCodeCompletionTrait
{$ENDIF}
    )
  private
    fInstance90: IOTACodeInsightManager90;
    /// <summary>
    ///   If disabled the wrapper is skipped but the code completion continues
    ///   to work.
    /// </summary>
    fWrapperEnabled: Boolean;
    fRunning: Boolean;
    /// <summary>
    ///   Cached wrapper instance
    /// </summary>
    [Unsafe] fCachedList: TSmartCodeInsightSymbolListWrapper;
    /// <summary>
    ///   Last instance to wrap
    /// </summary>
    fLastList: Pointer {IOTACodeInsightSymbolList};
    fCustomDrawCodeInsightViewer: INTACustomDrawCodeInsightViewer;
    fCachedViewer: IOTACodeInsightViewer;
    fLastViewer: Pointer {IOTACodeInsightViewer};
{$IFDEF WRAP_ALL}
    fPrimaryManager: IOTAPrimaryCodeInsightManager;
    fAutoCodeCompletionTrait: IOTAAutoCodeCompletionTrait;
{$ENDIF}
  protected
    procedure WrapViewer(var viewer: IOTACodeInsightViewer);
  public
    procedure AfterConstruction; override;
    destructor Destroy; override;

    // IOTACodeInsightManager100
    function GetName: string;
    function GetIDString: string;
    function GetEnabled: Boolean;
    procedure SetEnabled(Value: Boolean);
    function EditorTokenValidChars(PreValidating: Boolean): TSysCharSet;
    procedure AllowCodeInsight(var Allow: Boolean; const Key: Char);
    function PreValidateCodeInsight(const Str: string): Boolean;
    function IsViewerBrowsable(Index: Integer): Boolean;
    function GetMultiSelect: Boolean;
    procedure GetSymbolList(out SymbolList: IOTACodeInsightSymbolList);
    procedure OnEditorKey(Key: Char; var CloseViewer: Boolean; var Accept: Boolean);
    function HandlesFile(const AFileName: string): Boolean;
    function GetLongestItem: string;
    procedure GetParameterList(out ParameterList: IOTACodeInsightParameterList);
    procedure GetCodeInsightType(AChar: Char; AElement: Integer; out CodeInsightType: TOTACodeInsightType;
      out InvokeType: TOTAInvokeType);
    function InvokeCodeCompletion(HowInvoked: TOTAInvokeType; var Str: string): Boolean;
    function InvokeParameterCodeInsight(HowInvoked: TOTAInvokeType; var SelectedIndex: Integer): Boolean;
    procedure ParameterCodeInsightAnchorPos(var EdPos: TOTAEditPos);
    function ParameterCodeInsightParamIndex(EdPos: TOTAEditPos): Integer;
    function GetHintText(HintLine, HintCol: Integer): string;
    function GotoDefinition(out AFileName: string; out ALineNum: Integer; Index: Integer = -1): Boolean;
    procedure Done(Accepted: Boolean; out DisplayParams: Boolean);

    // IOTACodeInsightManager
    function GetOptionSetName: string;

    // IOTACodeInsightManager90
    function GetHelpInsightHtml: WideString;

    // INTACustomDrawCodeInsightViewer
    procedure DrawLine(Index: Integer; Canvas: TCanvas; var Rect: TRect;
      DrawingHintText: Boolean; DoDraw: Boolean; var DefaultDraw: Boolean);

{$IFDEF WRAP_ALL}
    // IOTAPrimaryCodeInsightManager
    property PrimaryManager: IOTAPrimaryCodeInsightManager
      read fPrimaryManager implements IOTAPrimaryCodeInsightManager;

    // IOTAAutoCodeCompletionTrait
    property AutoCodeCompletionTrait: IOTAAutoCodeCompletionTrait
      read fAutoCodeCompletionTrait implements IOTAAutoCodeCompletionTrait;
{$ENDIF}
  end;

  TIndex = record
    Score: Cardinal;
    Index: Integer;
  end;

  TIndexComparer = class(TInterfacedObject, IComparer<TIndex>)
  public
    function Compare(const Left, Right: TIndex): Integer;
  end;

  TSmartCodeInsightSymbolListWrapper = class(TInterfaceWrapper<IOTACodeInsightSymbolList>,
    IOTACodeInsightSymbolList, IOTACodeInsightSymbolList80, IPascalSymbolList)
  private class var
    fComparer: IComparer<TIndex>;
  private
    fInstance80: IOTACodeInsightSymbolList80;
    fFilter: string;
    fIndex: TArray<TIndex>;
    fSortEnabled: Boolean;
  protected
    function GetScore(const input, ident: string): Double;
    function GetIndex(index: Integer): Integer; inline;
    procedure ApplyNewFilter;
    function GetScoreValue(index: Integer): Double; inline;
  public
    procedure AfterConstruction; override;

    // IOTACodeInsightSymbolList
    procedure Clear;
    function GetCount: Integer;
    function GetSymbolIsReadWrite(I: Integer): Boolean;
    function GetSymbolIsAbstract(I: Integer): Boolean;
    function GetViewerSymbolFlags(I: Integer): TOTAViewerSymbolFlags;
    function GetViewerVisibilityFlags(I: Integer): TOTAViewerVisibilityFlags;
    function GetProcDispatchFlags(I: Integer): TOTAProcDispatchFlags;
    procedure SetSortOrder(const Value: TOTASortOrder);
    function GetSortOrder: TOTASortOrder;
    function FindIdent(const AnIdent: string): Integer;
    function FindSymIndex(const Ident: string; var Index: Integer): Boolean;
    procedure SetFilter(const FilterText: string);
    function GetSymbolText(Index: Integer): string;
    function GetSymbolTypeText(Index: Integer): string;
    function GetSymbolClassText(I: Integer): string;

    // IOTACodeInsightSymbolList80
    function GetSymbolDocumentation(I: Integer): string;

    // IPascalSymbolList
    function IsActive: Boolean;
  end;

var
  Plugin: TSmartCodeCompletion = nil;

implementation

procedure Hooked_GetViewer(const inst: Pointer; out viewer: IOTACodeInsightViewer);
begin
  Plugin.fGetViewer_Trampoline(inst, viewer);
  if Assigned(Plugin.Manager) then
    Plugin.Manager.WrapViewer(viewer);
end;

{ TSmartCodeCompletion }

constructor TSmartCodeCompletion.Create;
type
  TVersionInfo = record
    major: Integer;
    minor: Integer;
    patch: Integer;
    build: Integer;
  end;

  procedure GetVersionInfo(var versionInfo: TVersionInfo);
  var
    buffer: array[0..MAX_PATH] of Char;
    infoSize: DWORD;
    info: Pointer;
    valueSize: DWORD;
    value: PVSFixedFileInfo;
    dummy: DWORD;
  begin
    GetModuleFileName(hInstance, buffer, MAX_PATH);
    infoSize := GetFileVersionInfoSize(buffer, dummy);
    if infoSize <> 0 then
    begin
      GetMem(info, infoSize);
      try
        GetFileVersionInfo(buffer, 0, infoSize, info);
        VerQueryValue(info, '\', Pointer(value), valueSize);
        with value^ Do
        begin
          versionInfo.major := dwFileVersionMS shr 16;
          versionInfo.minor := dwFileVersionMS and $FFFF;
          versionInfo.patch := dwFileVersionLS shr 16;
          versionInfo.build := dwFileVersionLS and $FFFF;
        end;
      finally
        FreeMem(info, infoSize);
      end;
    end;
  end;

  function findManager(out manager: IOTACodeInsightManager; out index: Integer;
    const codeInsightServices: IOTACodeInsightServices; const id: string): Boolean;
  var
    i: Integer;
  begin
    for i := 0 to codeInsightServices.CodeInsightManagerCount - 1 do
    begin
      manager := codeInsightServices.CodeInsightManager[i];
      if manager.GetIDString = id then
      begin
        index := i;
        Exit(True);
      end;
    end;
    manager := nil;
    index := -1;
    Result := False;
  end;

const
  ProductName = 'SmartCodeCompletion %d.%d';
  ProductDescription = '';
var
  //icon: TIcon;
  fileVersion: TVersionInfo;
  productVersion: string;

  codeInsightServices: IOTACodeInsightServices;
  manager: IOTACodeInsightManager;
  index: Integer;
  adr: PPointer;
begin
  inherited Create;

  codeInsightServices := BorlandIDEServices as IOTACodeInsightServices;
  if not findManager(manager, index, codeInsightServices,
    'Borland.CodeInsight.Pascal') then
      Abort;

  fManager := TSmartCodeInsightManagerWrapper.Create(manager);
  // Do not call _AddRef keep only weak reference so the manager is released
  // when the CodeInsightServices need it to.
  codeInsightServices.RemoveCodeInsightManager(index);
  codeInsightServices.AddCodeInsightManager(fManager);

  // We need to hook the GetViewer to modify GetSelectedIndex used by
  // GetHelpInsightHtml.
  adr := PPointer(PNativeInt(codeInsightServices)^ + $10);
  fGetViewer_Trampoline := WriteProcess(adr, @Hooked_GetViewer);

  GetVersionInfo(fileVersion);
  productVersion := Format(ProductName, [
    fileVersion.major, fileVersion.minor, fileVersion.patch, fileVersion.build]);
  //fBitmap := LoadBitmap(hInstance, 'SmartCodeCompletionLogo');
  fAboutBoxIndex := (BorlandIDEServices as IOTAAboutBoxServices).AddPluginInfo(
    productVersion, ProductDescription, fBitmap);
  SplashScreenServices.AddPluginBitmap(productVersion, fBitmap);
end;

destructor TSmartCodeCompletion.Destroy;
var
  codeInsightServices: IOTACodeInsightServices;
  adr: PPointer;
begin
  codeInsightServices := BorlandIDEServices as IOTACodeInsightServices;
  if Assigned(fGetViewer_Trampoline) then
  begin
    adr := PPointer(PNativeInt(codeInsightServices)^ + $10);
    WriteProcess(adr, @fGetViewer_Trampoline);
  end;

  inherited;
end;

procedure TSmartCodeCompletion.ManagerFreed;
begin
  fManager := nil;
end;

function TSmartCodeCompletion.WriteProcess(const adr: PPointer;
  const data: Pointer): Pointer;
var
  memoryInfo: TMemoryBasicInformation;
  oldProtect: Cardinal;
begin
  Result := nil;
  if VirtualQuery(adr, memoryInfo, SizeOf(memoryInfo)) > 0 then
  begin
    if VirtualProtect(memoryInfo.BaseAddress, memoryInfo.RegionSize,
      PAGE_READWRITE, OldProtect) then
    begin
      Result := adr^;
      adr^ := data;
      VirtualProtect(memoryInfo.BaseAddress, memoryInfo.RegionSize,
        OldProtect, OldProtect);
    end;
  end;
end;

{ TSmartCodeInsightManagerWrapper }

procedure TSmartCodeInsightManagerWrapper.AfterConstruction;
begin
  fInstance.QueryInterface(IOTACodeInsightManager90, fInstance90);
  fInstance.QueryInterface(INTACustomDrawCodeInsightViewer, fCustomDrawCodeInsightViewer);
{$IFDEF WRAP_ALL}
  TryAddAggregate<IOTAPrimaryCodeInsightManager>(fInstance, fPrimaryManager,
    TSmartCodeInsightPrimaryManagerWrapper);
  {TryAddAggregate<INTACustomDrawCodeInsightViewer>(fInstance,
    fCustomDrawCodeInsightViewer,
    TSmartCodeInsightCustomDrawCodeInsightViewerWrapper);}
  TryAddAggregate<IOTAAutoCodeCompletionTrait>(fInstance,
    fAutoCodeCompletionTrait,
    TSmartCodeInsightAutoCodeCompletionTraitWrapper);
{$ENDIF}
{$IFDEF DEBUG}
  CheckImplementedInterfaces(fInstance as TObject, Self);
{$ENDIF}
  fWrapperEnabled := True;
  // Call after we're initialized to maintain RefCount during initialization
  inherited;
end;

procedure TSmartCodeInsightManagerWrapper.AllowCodeInsight(var Allow: Boolean;
  const Key: Char);
begin
  fInstance.AllowCodeInsight(Allow, Key);
  {$IFDEF DEBUG}Trace('AllowCodeInsight', BoolToStr(Allow, True) + ': ' + Key);{$ENDIF}
  fRunning := Allow and fWrapperEnabled;
end;

destructor TSmartCodeInsightManagerWrapper.Destroy;
begin
  // Release the weak reference
  if Assigned(Plugin) then
    Plugin.ManagerFreed;

  if Assigned(fCachedList) then
    fCachedList._Release;
  inherited;
end;

procedure TSmartCodeInsightManagerWrapper.Done(Accepted: Boolean;
  out DisplayParams: Boolean);
var
  inst: TObject;
  ctx: TRttiContext;
begin
  {$IFDEF DEBUG}Trace('Done', BoolToStr(Accepted, True));{$ENDIF}
  // Setting the fFilter fixes leftover text in method completion
  inst := fCachedList.Instance as TObject;
  ctx.GetType(inst.ClassType).GetField('FFilter').SetValue(inst,
    fCachedList.fFilter);

  fInstance.Done(Accepted, DisplayParams);
  // fRunning := False;
  // No need to clear the cache, Delphi use one persistent object anyway
  {if Assigned(fCachedList) then
  begin
    fCachedList._Release;
    fCachedList := nil;
  end;
  fLastList := nil;}
end;

procedure TSmartCodeInsightManagerWrapper.DrawLine(Index: Integer;
  Canvas: TCanvas; var Rect: TRect; DrawingHintText, DoDraw: Boolean;
  var DefaultDraw: Boolean);
begin
  if Assigned(fCustomDrawCodeInsightViewer) then
  begin
    Assert(Assigned(fCachedList));
    if Assigned(fCachedList) then
      Index := fCachedList.GetIndex(index);
    fCustomDrawCodeInsightViewer.DrawLine(Index, Canvas, Rect, DrawingHintText,
      DoDraw, DefaultDraw);
  end;
end;

function TSmartCodeInsightManagerWrapper.EditorTokenValidChars(
  PreValidating: Boolean): TSysCharSet;
begin
  Result := fInstance.EditorTokenValidChars(PreValidating)
end;

procedure TSmartCodeInsightManagerWrapper.GetCodeInsightType(AChar: Char;
  AElement: Integer; out CodeInsightType: TOTACodeInsightType;
  out InvokeType: TOTAInvokeType);
begin
  {$IFDEF DEBUG}Trace('GetCodeInsightType', AChar);{$ENDIF}
  fInstance.GetCodeInsightType(AChar, AElement, CodeInsightType, InvokeType);
end;

function TSmartCodeInsightManagerWrapper.GetEnabled: Boolean;
begin
  Result := fInstance.GetEnabled;
end;

function TSmartCodeInsightManagerWrapper.GetHelpInsightHtml: WideString;
begin
  if Assigned(fInstance90) then
    Result := fInstance90.GetHelpInsightHtml
  else
    Result := '';
  {$IFDEF DEBUG}Trace('GetHelpInsightHtml', Result);{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.GetHintText(HintLine,
  HintCol: Integer): string;
begin
  Result := fInstance.GetHintText(HintLine, HintCol);
  {$IFDEF DEBUG}Trace('GetHintText', Result);{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.GetIDString: string;
begin
  Result := fInstance.GetIDString;
end;

function TSmartCodeInsightManagerWrapper.GetLongestItem: string;
begin
  Result := fInstance.GetLongestItem;
end;

function TSmartCodeInsightManagerWrapper.GetMultiSelect: Boolean;
begin
  Result := fInstance.GetMultiSelect;
end;

function TSmartCodeInsightManagerWrapper.GetName: string;
begin
  Result := fInstance.GetName;
end;

function TSmartCodeInsightManagerWrapper.GetOptionSetName: string;
begin
  Result := fInstance.GetOptionSetName;
end;

procedure TSmartCodeInsightManagerWrapper.GetParameterList(
  out ParameterList: IOTACodeInsightParameterList);
begin
  {$IFDEF DEBUG}Trace('GetParameterList');{$ENDIF}
  fInstance.GetParameterList(ParameterList);
end;

procedure TSmartCodeInsightManagerWrapper.GetSymbolList(
  out SymbolList: IOTACodeInsightSymbolList);
begin
  // {$IFDEF DEBUG}Trace('GetSymbolList');{$ENDIF}
  fInstance.GetSymbolList(SymbolList);
  if fRunning then
  begin
    if fLastList = Pointer(SymbolList) then
    begin
      // {$IFDEF DEBUG}Trace('Reused from cache');{$ENDIF}
      Assert(Assigned(fCachedList));
    end
    else
    begin
      {$IFDEF DEBUG}Trace('New list');{$ENDIF}
      fLastList := Pointer(SymbolList);
      fCachedList := TSmartCodeInsightSymbolListWrapper.Create(SymbolList);
      fCachedList._AddRef;
    end;
    SymbolList := fCachedList;
  end;
end;

function TSmartCodeInsightManagerWrapper.GotoDefinition(out AFileName: string;
  out ALineNum: Integer; Index: Integer): Boolean;
begin
  Result := fInstance.GotoDefinition(AFileName, ALineNum, Index);
  {$IFDEF DEBUG}Trace('GotoDefinition', AFileName);{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.HandlesFile(
  const AFileName: string): Boolean;
begin
  Result := fInstance.HandlesFile(AFileName);
end;

function TSmartCodeInsightManagerWrapper.InvokeCodeCompletion(
  HowInvoked: TOTAInvokeType; var Str: string): Boolean;
begin
  Result := fInstance.InvokeCodeCompletion(HowInvoked, Str);
  {$IFDEF DEBUG}Trace('InvokeCodeCompletion', Str + ': ' + BoolToStr(Result, True));{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.InvokeParameterCodeInsight(
  HowInvoked: TOTAInvokeType; var SelectedIndex: Integer): Boolean;
begin
  Result := fInstance.InvokeParameterCodeInsight(HowInvoked, SelectedIndex);
  {$IFDEF DEBUG}Trace('InvokeParameterCodeInsight', BoolToStr(Result, True));{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.IsViewerBrowsable(
  Index: Integer): Boolean;
begin
  Result := fInstance.IsViewerBrowsable(Index);
end;

procedure TSmartCodeInsightManagerWrapper.OnEditorKey(Key: Char;
  var CloseViewer, Accept: Boolean);
begin
  {$IFDEF DEBUG}Trace('OnEditorKey', Key);{$ENDIF}
  fInstance.OnEditorKey(Key, CloseViewer, Accept);
end;

procedure TSmartCodeInsightManagerWrapper.ParameterCodeInsightAnchorPos(
  var EdPos: TOTAEditPos);
begin
  fInstance.ParameterCodeInsightAnchorPos(EdPos);
end;

function TSmartCodeInsightManagerWrapper.ParameterCodeInsightParamIndex(
  EdPos: TOTAEditPos): Integer;
begin
  Result := fInstance.ParameterCodeInsightParamIndex(EdPos);
  {$IFDEF DEBUG}Trace('ParameterCodeInsightParamIndex', IntToStr(Result));{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.PreValidateCodeInsight(
  const Str: string): Boolean;
begin
  {$IFDEF DEBUG}Trace('PreValidateCodeInsight', Str);{$ENDIF}
  Result := fInstance.PreValidateCodeInsight(Str);
end;

procedure TSmartCodeInsightManagerWrapper.SetEnabled(Value: Boolean);
begin
  fInstance.SetEnabled(Value);
end;

procedure TSmartCodeInsightManagerWrapper.WrapViewer(
  var viewer: IOTACodeInsightViewer);
begin
  if fLastViewer <> Pointer(viewer) then
  begin
    fCachedViewer := TSmartCodeInsightCodeInsightViewer.Create(viewer, Self);
    fLastViewer := Pointer(viewer);
  end;
  viewer := fCachedViewer;
end;

{ TSmartCodeInsightPrimaryManagerWrapper }

function TSmartCodeInsightPrimaryManagerWrapper.GetContext: TOTACodeCompletionContext;
begin
  Result := fInstance.GetContext;
end;

{ TSmartCodeInsightSymbolListWrapper }

procedure TSmartCodeInsightSymbolListWrapper.AfterConstruction;
begin
  if not Assigned(fComparer) then
    fComparer := TIndexComparer.Create;
  // fSortEnabled := True;

  inherited;
  fInstance.QueryInterface(IOTACodeInsightSymbolList80, fInstance80);
{$IFDEF DEBUG}
  CheckImplementedInterfaces(fInstance as TObject, Self);
{$ENDIF}
end;

procedure TSmartCodeInsightSymbolListWrapper.ApplyNewFilter;
var
  lCount, i: Integer;
  s: string;
begin
  // #2
  lCount := fInstance.Count;
  SetLength(fIndex, lCount);
  for i := 0 to lCount - 1 do
    with fIndex[i] do
  begin
    Index := i;
    s := fInstance.SymbolText[i];
    Score := Trunc(GetScore(fFilter, s) * High(Score));
  end;

  TArray.Sort<TIndex>(fIndex, fComparer);
end;

procedure TSmartCodeInsightSymbolListWrapper.Clear;
begin
  fInstance.Clear;
  fFilter := '';
  fIndex := nil;
  {$IFDEF DEBUG}Trace('Clear');{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.FindIdent(
  const AnIdent: string): Integer;
begin
  if Assigned(fIndex) then
  begin
    if not FindSymIndex(AnIdent, Result) then
      Result := -1;
  end
  else
    Result := fInstance.FindIdent(AnIdent);
  {$IFDEF DEBUG}Trace('FindIdent', AnIdent + ': ' + IntToStr(Result));{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.FindSymIndex(const Ident: string;
  var Index: Integer): Boolean;
var
  lCount, i, iBest: Integer;
  d, best: Double;
  s: string;
{$IFDEF DEBUG}

  procedure LogMe;
  begin
    if iBest >= 0 then
        s := fInstance.SymbolText[GetIndex(iBest)];
    Trace('FindSymIndex', Ident + ' > ' + s + ': ' + BoolToStr(Result, True)
      + ' @' + IntToStr(Index) + ' ' + FloatToStr(best * 100) + '%');
  end;

{$ENDIF}
begin
  // Do not call original it would find the correct symbol by leading string
  // comparison and return true even if the leading string doesn't match
  // completely.
  // Result := fInstance.FindSymIndex(Ident, Index);
  // if Result then
  //   Exit;

  if Ident = '' then
    Exit(False);

  lCount := Self.GetCount;
  if lCount = 0 then
    Exit(False);

  if fSortEnabled and (Ident = fFilter) then
  begin
    Index := 0;
{$IFDEF DEBUG}
    Result := True;
    best := GetScoreValue(0);
    iBest := Index;
    LogMe;
{$ENDIF}
    Exit(True);
  end;

  iBest := -1;
  best := -1;

  for i := 0 to lCount - 1 do
  begin
    s := fInstance.SymbolText[GetIndex(i)];
    d := GetScore(ident, s);
    if d > best then
    begin
      best := d;
      iBest := i;
    end;
  end;

  Result := iBest >= 0;
  if Result then
    Index := iBest;

  {$IFDEF DEBUG}LogMe;{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.GetCount: Integer;
begin
  Result := fInstance.GetCount;
  Assert((not Assigned(fIndex)) or (Length(fIndex) = Result));
  {$IFDEF DEBUG}Trace('GetCount', IntToStr(Result));{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.GetIndex(index: Integer): Integer;
begin
  if Assigned(fIndex) then
    Result := fIndex[index].Index
  else
    Result := index;
end;

function TSmartCodeInsightSymbolListWrapper.GetProcDispatchFlags(
  I: Integer): TOTAProcDispatchFlags;
begin
  Result := fInstance.GetProcDispatchFlags(GetIndex(I));
end;

function GetCamelCaseRatio(const ident, s: string): Double;

  function IsUpper(const c: Char): Boolean; inline;
  begin
     Result := (c < #$80) and (AnsiChar(c) in ['A'..'Z', '_']);
  end;

var
  iPos, sPos, remaining: Integer;
  c: Char;
begin
  // First character's case is ignored to correctly match camelCase identifiers.
  if UpCase(s[1]) <> UpCase(ident[1]) then
    Exit(0);

  iPos:=2;
  sPos:=2;

  while (iPos <= Length(ident)) and (sPos <= Length(s)) do
  begin
    c := s[sPos];
    if IsUpper(c) then
    begin
      if UpCase(ident[iPos]) <> c then
        Exit(0);
      Inc(iPos);
    end;
    Inc(sPos);
  end;

  // Did we consume all of ident chars?
  if iPos <= Length(ident) then
    Exit(0); // If not than this is not a prefix (nor complete match)

  remaining := 0;
  for sPos := sPos to Length(s) do
    if IsUpper(s[sPos]) then
      Inc(remaining);

  // The total score is computed to how many upper case characters are left in
  // the given suggestion (the CamelCase is prefix). If no characters are left
  // this gives exact match (1.0).
  // If this favors CamelCase prefixes over other matches some functions that
  // falls more steeply to zero needs to be selected.
  Result := iPos / (iPos + remaining);
end;

function TSmartCodeInsightSymbolListWrapper.GetScore(const input,
  ident: string): Double;
var
  d: Double;
begin
  Result := StringSimilarityRatio(input, ident, True);

  if StartsText(input, ident) then
  begin
    // Set to high rank while giving a bump to the prefix while also
    // maintaining similarity ratio at some degree.
    //d := 0.8 + (0.2 * Result);
    // Note: if this doesn't work compare the similarity as
    // `Length(input) / Length(ident)` that should put shortest macthes first.
    d := 0.8 + (0.2 * (Length(input) / Length(ident)));

    // Give function Result a little bump (usually over Reset function).
    if (Length(input) <= 3) and (Ident = 'Result') then
      d := d * 1.05;

    if d > Result then
    begin
      Result := d;
      // Don't break here, direct match should always win
    end;
  end;

  // Check CamelCase notations (#1)
  // Require at least two characters so we have at least some reference
  if Length(input) > 1 then
  begin
    d := GetCamelCaseRatio(input, ident);
    // Put shortest matches first
    d := (0.85 * d) + (0.15 * (Length(input) / Length(ident)));
    if d > Result then
      Result := d;
  end;
end;

function TSmartCodeInsightSymbolListWrapper.GetScoreValue(
  index: Integer): Double;
begin
  if Assigned(fIndex) then
    Result := fIndex[index].Score / High(fIndex[index].Score)
  else
    Result := -1;
end;

function TSmartCodeInsightSymbolListWrapper.GetSortOrder: TOTASortOrder;
begin
  Result := fInstance.GetSortOrder;
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolClassText(
  I: Integer): string;
begin
  Result := fInstance.GetSymbolClassText(GetIndex(I));
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolDocumentation(
  I: Integer): string;
begin
  if Assigned(fInstance80) then
    Result := fInstance80.GetSymbolDocumentation(GetIndex(I))
  else
    Result := '';
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolIsAbstract(
  I: Integer): Boolean;
begin
  Result := fInstance.GetSymbolIsAbstract(GetIndex(I));
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolIsReadWrite(
  I: Integer): Boolean;
begin
  Result := fInstance.GetSymbolIsReadWrite(GetIndex(I));
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolText(
  Index: Integer): string;
begin
  Result := fInstance.GetSymbolText(GetIndex(Index));
  {$IFDEF DEBUG}Trace('GetSymbolText', IntToStr(Index) + ','
    + IntToStr(GetIndex(Index)) + ': ' + Result + ' - '
    + FloatToStr(GetScoreValue(Index) * 100) + '%');{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolTypeText(
  Index: Integer): string;
begin
  Result := fInstance.GetSymbolTypeText(GetIndex(Index))
end;

function TSmartCodeInsightSymbolListWrapper.GetViewerSymbolFlags(
  I: Integer): TOTAViewerSymbolFlags;
begin
  Result := fInstance.GetViewerSymbolFlags(GetIndex(I));
end;

function TSmartCodeInsightSymbolListWrapper.GetViewerVisibilityFlags(
  I: Integer): TOTAViewerVisibilityFlags;
begin
  Result := fInstance.GetViewerVisibilityFlags(GetIndex(I));
end;

function TSmartCodeInsightSymbolListWrapper.IsActive: Boolean;
begin
  Result := (fInstance as IPascalSymbolList).IsActive;
end;

procedure TSmartCodeInsightSymbolListWrapper.SetFilter(
  const FilterText: string);
begin
  {$IFDEF DEBUG}Trace('SetFilter', FilterText);{$ENDIF}
  // fInstance.SetFilter(FilterText);
  // Show all items all the time FindSymIndex will find the proper item to focus
  fInstance.SetFilter('');
  if fFilter <> FilterText then
  begin
    fFilter := FilterText;
    if fSortEnabled and (fFilter <> '') then
      ApplyNewFilter
    else
      fIndex := nil;
  end;
end;

procedure TSmartCodeInsightSymbolListWrapper.SetSortOrder(
  const Value: TOTASortOrder);
begin
  fInstance.SetSortOrder(Value);
  {$IFDEF DEBUG}Trace('SetSortOrder');{$ENDIF}
end;

{ TSmartCodeInsightCustomDrawCodeInsightViewerWrapper }

procedure TSmartCodeInsightCustomDrawCodeInsightViewerWrapper.DrawLine(
  Index: Integer; Canvas: TCanvas; var Rect: TRect; DrawingHintText,
  DoDraw: Boolean; var DefaultDraw: Boolean);
begin
  fInstance.DrawLine(Index, Canvas, Rect, DrawingHintText, DoDraw, DefaultDraw);
end;

{ TIndexComparer }

function TIndexComparer.Compare(const Left, Right: TIndex): Integer;
begin
  // Greater values first
  if Left.Score < Right.Score then
    Result := 1
  else if Left.Score > Right.Score then
    Result := -1
  else begin
    // Maintain priority of the original index since it is related to current
    // scope. (Smaller values first)
    if Left.Index > Right.Index then
      Result := 1
    else if Left.Index < Right.Index then
      Result := -1
    else Result := 0;
  end;
end;

{ TSmartCodeInsightCodeInsightViewer }

constructor TSmartCodeInsightCodeInsightViewer.Create(
  const instance: IOTACodeInsightViewer;
  const manager: TSmartCodeInsightManagerWrapper);
begin
  inherited Create(instance);
  fManager := manager;
{$IFDEF DEBUG}
  CheckImplementedInterfaces(instance as TObject, Self);
{$ENDIF}
end;

function TSmartCodeInsightCodeInsightViewer.GetCloseKey: Char;
begin
  Result := fInstance.CloseKey;
end;

function TSmartCodeInsightCodeInsightViewer.GetIndex(Index: Integer): Integer;
begin
  Assert(Assigned(fManager.fCachedList));
  if Assigned(fManager.fCachedList) then
    Result := fManager.fCachedList.GetIndex(Index)
  else
    Result := Index;
end;

function TSmartCodeInsightCodeInsightViewer.GetIsValidSelection: Boolean;
begin
  Result := fInstance.IsValidSelection;
end;

function TSmartCodeInsightCodeInsightViewer.GetItemCount: Integer;
begin
  Result := fInstance.ItemCount;
end;

function TSmartCodeInsightCodeInsightViewer.GetManagerIsValidSelection(
  const Mgr: IOTACodeInsightManager; Index: Integer): Boolean;
begin
  Result := fInstance.GetManagerIsValidSelection(Mgr, GetIndex(Index));
end;

function TSmartCodeInsightCodeInsightViewer.GetManagerIsValidSelection(
  const Mgr: IOTACodeInsightManager): Boolean;
begin
  Result := fInstance.GetManagerIsValidSelection(Mgr)
end;

function TSmartCodeInsightCodeInsightViewer.GetManagerSelectedIndex(
  const Mgr: IOTACodeInsightManager): Integer;
begin
  Result := GetIndex(fInstance.GetManagerSelectedIndex(Mgr));
end;

function TSmartCodeInsightCodeInsightViewer.GetSelected(
  Index: Integer): Boolean;
begin
  Result := fInstance.Selected[GetIndex(Index)];
end;

function TSmartCodeInsightCodeInsightViewer.GetSelectedIndex: Integer;
begin
  Result := GetIndex(fInstance.SelectedIndex);
end;

function TSmartCodeInsightCodeInsightViewer.GetSelectedString: string;
begin
  Result := fInstance.SelectedString;
end;

{ TSmartCodeInsightAutoCodeCompletionTraitWrapper }

function TSmartCodeInsightAutoCodeCompletionTraitWrapper.GetAutoCodeCompleteEnabled: Boolean;
begin
  Result := Instance.GetAutoCodeCompleteEnabled;
end;

function TSmartCodeInsightAutoCodeCompletionTraitWrapper.IdentFirstSet: TSysCharSet;
begin
  Result := Instance.IdentFirstSet;
end;

function TSmartCodeInsightAutoCodeCompletionTraitWrapper.IdentPrevSet: TSysCharSet;
begin
  Result := Instance.IdentPrevSet;
end;

end.

// Unknown interfaces
-non public
?generic interfaces
+added

TSmartCodeInsightManagerWrapper:
-Missing implementation (IOTACodeCompletionHistory {D9BE3F58-D9D3-4CA8-96BE-ED069AA05912})
-Missing implementation (IOTAAutoCodeCompletionTrait {A84C137A-F375-478E-AD0C-479B707A22EB})
+Missing implementation (IOTACodeInsightManager100 {BA5B444A-6E78-4A79-BF05-E184C1132B30})
Missing implementation (IOTACodeInsightParameterList {99B6A644-3E97-48A1-9758-0A5FE94767C7})
Missing implementation (IOTACodeInsightParameterList100 {EC7B37F6-8AB8-4B09-85A4-AA53D5856C0F})
Missing implementation (IOTACodeInsightParamQuery {B1842926-C7F7-4869-B55A-CFDB6BF705B5})
?Missing implementation (ISupportErrorInfo {DF0B3D60-548F-101B-8E65-08002B2BD119}) OLE - ActiveX
?Missing implementation (IAmbientDispatch {00020400-0000-0000-C000-000000000046}) OLE - AxCtrls

