{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

unit Levenshtein;

interface

uses SysUtils, Math, StrUtils;


/// <summary>
///   Computes the Levenshtein distance is a string metric for measuring the
///   difference between two sequences. Informally, the Levenshtein distance
///   between two words is the minimum number of single-character edits (i.e.
///   insertions, deletions or substitutions) required to change one word into
///   the other.
///   http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Delphi
/// </summary>
function LevenshteinDistance(Word1, Word2: String): integer;

// Optimized version
// http://www.delphiarea.com/articles/how-to-match-two-strings-approximately/
// Courtesy of Kambiz R. Khojasteh (http://www.delphiarea.com/)
function DamerauLevenshteinDistance(const Str1, Str2: String): Integer;
/// <summary>
///   The return value of <c>StringSimilarityRatio</c> function is a
///   floating-point number between <c>0</c> (zero) and <c>1</c> (one), where <c>
///   0</c> means not similar at all and <c>1</c> means equal strings.
/// </summary>
function StringSimilarityRatio(const Str1, Str2: String; IgnoreCase: Boolean): Double;

implementation

function LevenshteinDistance(Word1, Word2: String): integer;
var lev : array of array of integer;
    i,j : integer;
begin
   // Word1 := LowerCase(Word1);
   // Word2 := LowerCase(Word2);

  // If the words are identical, do nothing
  if Word1 = Word2 then
  begin
    result := 0;
    exit;
  end;

  SetLength(lev, length(Word1) + 1);
  for i := low(lev) to high(lev) do
    setLength(lev[i], length(Word2) + 1);

  for i := low(lev) to high(lev) do lev[i][0] := i;
  for j := low(lev[low(lev)]) to high(lev[low(lev)]) do lev[0][j] := j;

  for i := low(lev)+1 to high(lev) do
    for j := low(lev[i])+1 to high(lev[i]) do
      lev[i][j] := min(min(lev[i-1][j] + 1,lev[i][j-1] + 1)
                      ,lev[i-1][j-1] + ifthen(Word1[i] = Word2[j], 0, 1));

  result := lev[length(word1)][length(word2)];
end;

function DamerauLevenshteinDistance(const Str1, Str2: String): Integer;

  function Min(const A, B, C: Integer): Integer;
  begin
    Result := A;
    if B < Result then
      Result := B;
    if C < Result then
      Result := C;
  end;

var
  LenStr1, LenStr2: Integer;
  I, J, T, Cost, PrevCost: Integer;
  pStr1, pStr2, S1, S2: PChar;
  D: PIntegerArray;
begin
  LenStr1 := Length(Str1);
  LenStr2 := Length(Str2);

  // save a bit memory by making the second index points to the shorter string
  if LenStr1 < LenStr2 then
  begin
    T := LenStr1;
    LenStr1 := LenStr2;
    LenStr2 := T;
    pStr1 := PChar(Str2);
    pStr2 := PChar(Str1);
  end
  else
  begin
    pStr1 := PChar(Str1);
    pStr2 := PChar(Str2);
  end;

  // bypass leading identical characters
  while (LenStr2 <> 0) and (pStr1^ = pStr2^) do
  begin
    Inc(pStr1);
    Inc(pStr2);
    Dec(LenStr1);
    Dec(LenStr2);
  end;

  // bypass trailing identical characters
  while (LenStr2 <> 0) and ((pStr1 + LenStr1 - 1)^ = (pStr2 + LenStr2 - 1)^) do
  begin
    Dec(LenStr1);
    Dec(LenStr2);
  end;

  // is the shorter string empty? so, the edit distance is length of the longer one
  if LenStr2 = 0 then
  begin
    Result := LenStr1;
    Exit;
  end;

  // calculate the edit distance
  // using global cache as suggested by some implementators should not be needed
  // in Delphi FastMM should do the job for us considering we'll use small
  // memory blocks most of the time.
  GetMem(D, (LenStr2 + 1) * SizeOf(Integer));

  for I := 0 to LenStr2 do
    D[I] := I;

  S1 := pStr1;
  for I := 1 to LenStr1 do
  begin
    PrevCost := I - 1;
    Cost := I;
    S2 := pStr2;
    for J := 1 to LenStr2 do
    begin
      if (S1^ = S2^) or ((I > 1) and (J > 1) and (S1^ = (S2 - 1)^) and (S2^ = (S1 - 1)^)) then
        Cost := PrevCost
      else
        Cost := 1 + Min(Cost, PrevCost, D[J]);
      PrevCost := D[J];
      D[J] := Cost;
      Inc(S2);
    end;
    Inc(S1);
  end;
  Result := D[LenStr2];
  FreeMem(D);
end;

function StringSimilarityRatio(const Str1, Str2: String; IgnoreCase: Boolean): Double;
var
  MaxLen: Integer;
  Distance: Integer;
begin
  Result := 1.0;
  if Length(Str1) > Length(Str2) then
	MaxLen := Length(Str1)
  else
	MaxLen := Length(Str2);
  if MaxLen <> 0 then
  begin
	if IgnoreCase then
	  Distance := DamerauLevenshteinDistance(LowerCase(Str1), LowerCase(Str2))
	else
	  Distance := DamerauLevenshteinDistance(Str1, Str2);
	Result := Result - (Distance / MaxLen);
  end;
end;

end.
