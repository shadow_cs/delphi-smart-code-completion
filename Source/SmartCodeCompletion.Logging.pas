{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

unit SmartCodeCompletion.Logging;

interface

{$I SmartCodeCompletion.inc}

uses
  SysUtils,
  Rtti,
  ToolsAPI,
  Windows;

{$IFDEF DEBUG}
procedure Trace(const name: string; const args: string = '');
procedure DumpClass(const inst: TObject);
{$ENDIF}

implementation

{$IFDEF DEBUG}
procedure Log(const msg: string);
begin
  OutputDebugString(PChar(msg));
  if (GetKeyState(VK_SCROLL) and 1 = 1) then
  begin
    (BorlandIDEServices as IOTAMessageServices).AddToolMessage('', msg, 'SmartCC',
      0, 0);
  end;
end;

procedure Trace(const name: string; const args: string = '');
var
  s: string;
begin
  s := 'Trace: ' + name + '(' + args + ')';
  Log(s);
end;

procedure DumpClass(const inst: TObject);

  function GetName(const obj: TRttiNamedObject): string;
  begin
    if Assigned(obj) then
      Result := obj.Name
    else
      Result := '(nil)';
  end;

  function GetValue(const field: TRttiField): string;
  begin
    if Assigned(field.FieldType) then
      Result := field.GetValue(inst).ToString
    else
      Result := '(unavailable)';
  end;

var
  ctx: TRttiContext;
  clazz: TRttiInstanceType;
  intf: TRttiInterfaceType;
  field: TRttiField;
  method: TRttiMethod;
begin
  clazz := ctx.GetType(inst.ClassType).AsInstance;
  while Assigned(clazz) do
  begin
    Log(clazz.Name);
    for intf in clazz.GetDeclaredImplementedInterfaces do
      Log(' implements ' + intf.Name + ': ' + GUIDToString(intf.GUID));
    for field in clazz.GetDeclaredFields do
      Log(field.Name + ': ' + GetName(field.FieldType) + ' = ' + GetValue(field));
    for method in clazz.GetDeclaredMethods do
      Log(method.ToString);

    clazz := clazz.BaseType;
  end;
end;
{$ENDIF}

end.
