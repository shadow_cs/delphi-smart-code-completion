@echo off
  echo 1 rsvars.bat:  %1
  echo 2 .dproj file: %2
  echo 3 config:      %3
  echo 4 platform:    %4
  echo 5 pause:       "%5"
  call %1
setlocal
  set DprojDirectory=%~dp2
  set DprojDirectory=%DprojDirectory:~0,-1%
  set DprojDelphi=%~nx2
  echo %DprojDelphi%
  if not exist Logs\ mkdir Logs
  for /f "tokens=2,4" %%c in ('echo %~4 %~3') do set buildlog="Logs\%DprojDelphi%.%%c.%%d.MSBuildLog.txt"
  echo   build log:   %buildlog%
  %FrameworkDir%\msbuild.exe /nologo %2 /target:build /p:DCC_BuildAllUnits=true /p:%3 /p:%4 /l:FileLogger,Microsoft.Build.Engine;logfile=%buildlog%
  set RESULT=%ERRORLEVEL%
  if not "%5"=="" pause
  if not "%RESULT%"=="0" (
    endlocal
    EXIT /B 1
  )
endlocal
